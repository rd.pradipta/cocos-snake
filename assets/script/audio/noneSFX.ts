import { _decorator } from "cc";
import { ASSET_KEY } from "../lib/enum/asset";
import { BaseAudio } from "../lib/audio/baseAudio";
const { ccclass } = _decorator;

@ccclass("SilenceSFX")
export class SilenceSFX extends BaseAudio {
  constructor() {
    super("SilenceSFX", ASSET_KEY.AUDIO_SILENCE_SFX);
  }
}
