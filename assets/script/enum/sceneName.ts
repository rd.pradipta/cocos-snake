export enum SCENE_KEY {
  LOADIMG_SCENE = "loadingScene",
  TITLE_SCENE = "titleScene",
  GAME_SCENE = "gameScene",
}
