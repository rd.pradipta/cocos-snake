export enum TOUCH_EVENT {
  TOUCH_START = 'touch_start',
  TOUCH_MOVE = 'touch_move',
  TOUCH_END = 'touch_end',
  TOUCH_CANCEL = 'touch_cancel',
}
