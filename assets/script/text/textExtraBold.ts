import { _decorator } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseText } from '../lib/text/baseText';
const { ccclass } = _decorator;

@ccclass('TextExtraBold')
export class TextExtraBold extends BaseText {
  constructor() {
    super('TextExtraBold', ASSET_KEY.FONT_SHOPEE_2021_EXTRA_BOLD);
  }
}
