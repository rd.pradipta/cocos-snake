import { _decorator } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseText } from '../lib/text/baseText';
const { ccclass } = _decorator;

@ccclass('TextSemiBold')
export class TextSemiBold extends BaseText {
  constructor() {
    super('TextSemiBold', ASSET_KEY.FONT_SHOPEE_2021_SEMIBOLD);
  }
}
