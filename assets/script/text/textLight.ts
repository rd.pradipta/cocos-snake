import { _decorator } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseText } from '../lib/text/baseText';
const { ccclass } = _decorator;

@ccclass('TextLight')
export class TextLight extends BaseText {
  constructor() {
    super('TextLight', ASSET_KEY.FONT_SHOPEE_2021_LIGHT);
  }
}
