import { _decorator } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseText } from '../lib/text/baseText';
const { ccclass } = _decorator;

@ccclass('TextRegular')
export class TextRegular extends BaseText {
  constructor() {
    super('TextRegular', ASSET_KEY.FONT_SHOPEE_2021_REGULAR);
  }
}
