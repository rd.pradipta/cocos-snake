import { _decorator } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseText } from '../lib/text/baseText';
const { ccclass } = _decorator;

@ccclass('TextBold')
export class TextBold extends BaseText {
  constructor() {
    super('TextBold', ASSET_KEY.FONT_SHOPEE_2021_BOLD);
  }
}
