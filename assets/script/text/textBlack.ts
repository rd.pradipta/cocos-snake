import { _decorator } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseText } from '../lib/text/baseText';
const { ccclass } = _decorator;

@ccclass('TextBlack')
export class TextBlack extends BaseText {
  constructor() {
    super('TextBlack', ASSET_KEY.FONT_SHOPEE_2021_BLACK);
  }
}
