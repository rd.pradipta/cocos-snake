import { _decorator } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseText } from '../lib/text/baseText';
const { ccclass } = _decorator;

@ccclass('TextMedium')
export class TextMedium extends BaseText {
  constructor() {
    super('TextMedium', ASSET_KEY.FONT_SHOPEE_2021_MEDIUM);
  }
}
