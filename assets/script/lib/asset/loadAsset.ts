import { ASSET_EXTENSION, ASSET_KEY, ASSET_TYPE } from "../enum/asset";
import { AssetConfig } from "../interface/asset";

export function getAssets(): AssetConfig[] {
  const assets = new Array<AssetConfig>();

  // Audios (SFX)
  assets.push({
    key: ASSET_KEY.AUDIO_BUTTON_SFX,
    type: ASSET_TYPE.AUDIO_SFX,
    ext: ASSET_EXTENSION.MP3,
    url: "audio/sfx/button",
  });
  assets.push({
    key: ASSET_KEY.AUDIO_CRASH_SFX,
    type: ASSET_TYPE.AUDIO_SFX,
    ext: ASSET_EXTENSION.MP3,
    url: "audio/sfx/crash",
  });
  assets.push({
    key: ASSET_KEY.AUDIO_EAT_SFX,
    type: ASSET_TYPE.AUDIO_SFX,
    ext: ASSET_EXTENSION.MP3,
    url: "audio/sfx/eat",
  });
  assets.push({
    key: ASSET_KEY.AUDIO_SILENCE_SFX,
    type: ASSET_TYPE.AUDIO_SFX,
    ext: ASSET_EXTENSION.MP3,
    url: "audio/sfx/silence",
  });
  assets.push({
    key: ASSET_KEY.AUDIO_TURN_SFX,
    type: ASSET_TYPE.AUDIO_SFX,
    ext: ASSET_EXTENSION.MP3,
    url: "audio/sfx/turn",
  });

  // Audios (Soundtrack)
  assets.push({
    key: ASSET_KEY.AUDIO_BACKGROUND_SOUNDTRACK,
    type: ASSET_TYPE.AUDIO_SOUNDTRACK,
    ext: ASSET_EXTENSION.MP3,
    url: "audio/soundtrack/bg-music",
  });

  // Fonts
  assets.push({
    key: ASSET_KEY.FONT_SHOPEE_2021_BLACK,
    type: ASSET_TYPE.FONT,
    ext: ASSET_EXTENSION.TTF,
    url: "font/Shopee2021-Black",
  });
  assets.push({
    key: ASSET_KEY.FONT_SHOPEE_2021_BOLD,
    type: ASSET_TYPE.FONT,
    ext: ASSET_EXTENSION.TTF,
    url: "font/Shopee2021-Bold",
  });
  assets.push({
    key: ASSET_KEY.FONT_SHOPEE_2021_EXTRA_BOLD,
    type: ASSET_TYPE.FONT,
    ext: ASSET_EXTENSION.TTF,
    url: "font/Shopee2021-ExtraBold",
  });
  assets.push({
    key: ASSET_KEY.FONT_SHOPEE_2021_LIGHT,
    type: ASSET_TYPE.FONT,
    ext: ASSET_EXTENSION.TTF,
    url: "font/Shopee2021-Light",
  });
  assets.push({
    key: ASSET_KEY.FONT_SHOPEE_2021_MEDIUM,
    type: ASSET_TYPE.FONT,
    ext: ASSET_EXTENSION.TTF,
    url: "font/Shopee2021-Medium",
  });
  assets.push({
    key: ASSET_KEY.FONT_SHOPEE_2021_REGULAR,
    type: ASSET_TYPE.FONT,
    ext: ASSET_EXTENSION.TTF,
    url: "font/Shopee2021-Regular",
  });
  assets.push({
    key: ASSET_KEY.FONT_SHOPEE_2021_SEMIBOLD,
    type: ASSET_TYPE.FONT,
    ext: ASSET_EXTENSION.TTF,
    url: "font/Shopee2021-SemiBold",
  });

  // Images (Sprite)
  assets.push({
    key: ASSET_KEY.IMG_APPLE_SPRITE,
    type: ASSET_TYPE.IMG_SPRITE,
    ext: ASSET_EXTENSION.PNG,
    url: "image/sprite/sprite_apple",
  });
  assets.push({
    key: ASSET_KEY.IMG_LOGO_SPRITE,
    type: ASSET_TYPE.IMG_SPRITE,
    ext: ASSET_EXTENSION.PNG,
    url: "image/sprite/sprite_logo_shopee_ular",
  });
  assets.push({
    key: ASSET_KEY.IMG_SOUND_OFF_SPRITE,
    type: ASSET_TYPE.IMG_SPRITE,
    ext: ASSET_EXTENSION.PNG,
    url: "image/sprite/sprite_sound_off",
  });
  assets.push({
    key: ASSET_KEY.IMG_SOUND_ON_SPRITE,
    type: ASSET_TYPE.IMG_SPRITE,
    ext: ASSET_EXTENSION.PNG,
    url: "image/sprite/sprite_sound_on",
  });
  assets.push({
    key: ASSET_KEY.IMG_TROPHY_SPRITE,
    type: ASSET_TYPE.IMG_SPRITE,
    ext: ASSET_EXTENSION.PNG,
    url: "image/sprite/sprite_trophy",
  });
  assets.push({
    key: ASSET_KEY.IMG_WHITE_BOX_SPRITE,
    type: ASSET_TYPE.IMG_SPRITE,
    ext: ASSET_EXTENSION.PNG,
    url: "image/sprite/sprite_white_box",
  });

  // Images (Spritesheet)
  assets.push({
    key: ASSET_KEY.IMG_SNAKE_SPRITESHEET,
    type: ASSET_TYPE.IMG_SPRITESHEET,
    ext: ASSET_EXTENSION.PNG,
    url: "image/spritesheet/spritesheet_round",
    config: {
      frameWidth: 96,
      frameHeight: 96,
      paddingX: 1,
    },
  });
  assets.push({
    key: ASSET_KEY.IMG_KEYPAD_SPRITESHEET,
    type: ASSET_TYPE.IMG_SPRITESHEET,
    ext: ASSET_EXTENSION.PNG,
    url: "image/spritesheet/spritesheet_keypad",
    config: {
      frameWidth: 124,
      frameHeight: 124,
      paddingX: 20,
      paddingY: 16,
    },
  });
  assets.push({
    key: ASSET_KEY.IMG_TILE_SPRITESHEET,
    type: ASSET_TYPE.IMG_SPRITESHEET,
    ext: ASSET_EXTENSION.PNG,
    url: "image/spritesheet/spritesheet_tile",
    config: {
      frameWidth: 48,
      frameHeight: 48,
    },
  });
  assets.push({
    key: ASSET_KEY.IMG_WALL_SPRITE,
    type: ASSET_TYPE.IMG_SPRITE,
    ext: ASSET_EXTENSION.PNG,
    url: "image/sprite/sprite_wall",
  });

  return assets;
}
