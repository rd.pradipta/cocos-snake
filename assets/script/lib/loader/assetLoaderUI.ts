import { _decorator, Component, RichText } from "cc";
import { LoadingBar } from "./loadingBar";
const { ccclass, property } = _decorator;

@ccclass("AssetLoaderUI")
export class AssetLoaderUI extends Component {
  @property(RichText)
  public percentLoadText?: RichText | null;

  @property(RichText)
  public loadingText?: RichText | null;

  @property(LoadingBar)
  public readonly loadingBar?: LoadingBar | null;

  public updateText(progress: number, key?: string) {
    const { percentLoadText, loadingText } = this;
    const progressPercent = Math.floor(progress * 100);

    this.loadingBar?.drawInnerGraphics(progressPercent);

    if (percentLoadText) {
      percentLoadText.string = `${progressPercent}%`;
    }

    if (loadingText) {
      switch (progressPercent) {
        case 100: {
          loadingText.string = "WELCOME!";
          break;
        }

        case 0: {
          loadingText.string = "Loading...";
          break;
        }
      }
    }
  }
}
