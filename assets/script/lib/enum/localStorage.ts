export enum LOCAL_STORAGE_KEY {
  HIGH_SCORE = "high_score",
  AUDIO_STATE = "audio_state",
}
