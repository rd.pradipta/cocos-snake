export enum ASSET_EXTENSION {
  PNG = '.png',
  MP3 = '.mp3',
  TTF = '.ttf',
  META = '.meta',
}

export enum ASSET_KEY {
  // Images (Sprite)
  IMG_APPLE_SPRITE = 'apple_sprite',
  IMG_LOGO_SPRITE = 'logo_sprite',
  IMG_SOUND_OFF_SPRITE = 'sound_off_sprite',
  IMG_SOUND_ON_SPRITE = 'sound_on_sprite',
  IMG_TROPHY_SPRITE = 'trophy_sprite',
  IMG_WHITE_BOX_SPRITE = 'white_box_sprite',
  IMG_WALL_SPRITE = 'wall_sprite',

  // Images (Spritesheet)
  IMG_SNAKE_SPRITESHEET = 'snake_spritesheet',
  IMG_ROUND_SPRITESHEET = 'round_spritesheet',
  IMG_KEYPAD_SPRITESHEET = 'keypad_spritesheet',
  IMG_TILE_SPRITESHEET = 'tile_spritesheet',

  // Music
  AUDIO_BACKGROUND_SOUNDTRACK = 'background_soundtrack',

  // SFX (Sound Effects)
  AUDIO_BUTTON_SFX = 'button_sfx',
  AUDIO_CRASH_SFX = 'crash_sfx',
  AUDIO_EAT_SFX = 'eat_sfx',
  AUDIO_TURN_SFX = 'turn_sfx',
  AUDIO_SILENCE_SFX = 'silence_sfx',

  // Fonts
  FONT_SHOPEE_2021_BLACK = 'shopee_2021_black',
  FONT_SHOPEE_2021_BOLD = 'shopee_2021_bold',
  FONT_SHOPEE_2021_EXTRA_BOLD = 'shopee_2021_extra_bold',
  FONT_SHOPEE_2021_LIGHT = 'shopee_2021_light',
  FONT_SHOPEE_2021_MEDIUM = 'shopee_2021_medium',
  FONT_SHOPEE_2021_REGULAR = 'shopee_2021_regular',
  FONT_SHOPEE_2021_SEMIBOLD = 'shopee_2021_semibold',

  // Others
  TRANSITION_SCREEN = 'transition_screen',
}

export enum ASSET_TYPE {
  IMG_SPRITE = 'sprite',
  IMG_SPRITESHEET = 'spritesheet',
  AUDIO_SFX = 'sfx',
  AUDIO_SOUNDTRACK = 'soundtrack',
  FONT = 'font',
}
