import { _decorator, Component, Node } from "cc";
import { BaseText } from "../lib/text/baseText";
const { ccclass, property } = _decorator;

@ccclass("PlayStageHeader")
export class PlayStageHeader extends Component {
  @property(BaseText)
  public readonly scoreText?: BaseText;

  @property(BaseText)
  public readonly highScoreText?: BaseText;

  public show() {
    this.node.active = true;
  }

  public hide() {
    this.node.active = false;
  }

  public updateScore(score: number) {
    this.scoreText?.setText(score.toString());
  }

  public updateHighScore(score: number) {
    this.highScoreText?.setText(score.toString());
  }
}
