import { Button, _decorator, Node } from "cc";
import { TOUCH_EVENT } from "../enum/touchEvent";
const { ccclass } = _decorator;

@ccclass("BaseButton")
export class BaseButton extends Button {
  start() {
    this.registerTouchEvent();
  }
  private registerTouchEvent() {
    this.node.on(Node.EventType.TOUCH_END, () => {
      this.node.emit(TOUCH_EVENT.TOUCH_END);
    });
  }
  public unregisterTouchEvent() {
    this.node.off(Node.EventType.TOUCH_END);
  }
}
