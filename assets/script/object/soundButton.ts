import { _decorator } from "cc";
import { TOUCH_EVENT } from "../enum/touchEvent";
import {
  getSoundStateFromLocalStorage,
  updateLocalStorageSoundState,
} from "../lib/util/localStorage";
import { SoundButtonSprite } from "../sprite/soundButtonSprite";
import { BaseButton } from "./baseButton";
const { ccclass, property } = _decorator;

@ccclass("SoundButton")
export class SoundButton extends BaseButton {
  @property(SoundButtonSprite)
  public readonly soundButtonSprite?: SoundButtonSprite;

  start() {
    super.start();
    this.updateSoundButtonSprite();
    this.node.on(TOUCH_EVENT.TOUCH_END, () => {
      this.toggle();
    });
  }

  private toggle() {
    updateLocalStorageSoundState(!getSoundStateFromLocalStorage());
    this.updateSoundButtonSprite();
  }

  private updateSoundButtonSprite() {
    const state = getSoundStateFromLocalStorage();
    if (state) this.soundButtonSprite?.setOn();
    else this.soundButtonSprite?.setOff();
  }
}
