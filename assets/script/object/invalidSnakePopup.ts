import { _decorator, Color } from "cc";
import { TOUCH_EVENT } from "../enum/touchEvent";
import { INVALID_SNAKE_POP_UP_EVENT } from "../enum/invalidSnakePopup";
import { BasePopup } from "../lib/popup/basePopup";
import { TransitionScreen } from "../sprite/transitionScreen";
import { BaseButton } from "./baseButton";
const { ccclass, property } = _decorator;

@ccclass("InvalidSnakePopup")
export class InvalidSnakePopup extends BasePopup {
  @property(BaseButton)
  public readonly cancelButton?: BaseButton;

  @property(TransitionScreen)
  public readonly darkScreen?: TransitionScreen;

  public show() {
    super.show();
    this.darkScreen?.fadeIn(0.3, Color.BLACK, 122);
  }

  public registerTouchEvent() {
    const { cancelButton } = this;

    cancelButton?.node.on(TOUCH_EVENT.TOUCH_END, () => {
      this.node.emit(INVALID_SNAKE_POP_UP_EVENT.CANCEL);
    });
  }

  public unregisterTouchEvent() {
    const { cancelButton } = this;
    cancelButton?.node.off(TOUCH_EVENT.TOUCH_END);
  }
}
