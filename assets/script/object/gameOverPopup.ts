import { _decorator, Component, Node, Color } from "cc";
import { TOUCH_EVENT } from "../enum/touchEvent";
import { GAME_OVER_EVENT } from "../enum/gameOverPopup";
import { BasePopup } from "../lib/popup/basePopup";
import { BaseText } from "../lib/text/baseText";
import { TransitionScreen } from "../sprite/transitionScreen";
import { BaseButton } from "./baseButton";
const { ccclass, property } = _decorator;

@ccclass("GameOverPopup")
export class GameOverPopup extends BasePopup {
  @property(BaseButton)
  public readonly playAgainButton?: BaseButton;

  @property(BaseButton)
  public readonly cancelButton?: BaseButton;

  @property(BaseText)
  public readonly scoreText?: BaseText;

  @property(TransitionScreen)
  public readonly darkScreen?: TransitionScreen;

  public show() {
    super.show();
    this.darkScreen?.fadeIn(0.3, Color.BLACK, 122);
  }

  public registerTouchEvent() {
    const { cancelButton, playAgainButton } = this;

    cancelButton?.node.on(TOUCH_EVENT.TOUCH_END, () => {
      this.node.emit(GAME_OVER_EVENT.CANCEL);
    });

    playAgainButton?.node.on(TOUCH_EVENT.TOUCH_END, () => {
      this.node.emit(GAME_OVER_EVENT.PLAY_AGAIN);
    });
  }

  public unregisterTouchEvent() {
    const { cancelButton, playAgainButton } = this;

    cancelButton?.node.off(TOUCH_EVENT.TOUCH_END);

    playAgainButton?.node.off(TOUCH_EVENT.TOUCH_END);
  }

  public setScore(score: number) {
    this.scoreText?.setText(Math.round(score).toString());
  }
}
