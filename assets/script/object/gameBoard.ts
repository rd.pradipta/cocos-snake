import { _decorator, Component, Node, math, v2, instantiate } from "cc";
import { getTileType, TILE_TYPE } from "../enum/tile";
import { Tile } from "../scene/gameScene/interface/tile";
import { BaseTileSprite } from "../lib/sprite/baseTileSprite";
import { AppleSprite } from "../scene/gameScene/sprite/appleSprite";
import { TileSprite } from "../scene/gameScene/sprite/tileSprite";
import { WallSprite } from "../scene/gameScene/sprite/wallSprite";
import { Snake } from "./snake";
const { ccclass, property } = _decorator;

@ccclass("GameBoard")
export class GameBoard extends Component {
  @property(TileSprite)
  public readonly tileSprite?: BaseTileSprite;

  @property(WallSprite)
  public readonly wallSprite?: BaseTileSprite;

  @property(AppleSprite)
  public readonly appleSprite?: AppleSprite;

  @property(Node)
  public readonly tileNode?: Node;

  private readonly tileSize = 28;

  public tileBoard = new Array<Array<Tile>>();

  private fruits = new Array<{ node: Node; index: math.Vec2 }>();

  constructor() {
    super("GameBoard");
  }

  public generateBoardFromLevelConfig(levelConfig: Array<Array<number>>) {
    this.tileBoard = this.getBoardFromLevelConfig(levelConfig);
  }

  private getBoardFromLevelConfig(levelConfig: Array<Array<number>>) {
    return levelConfig.map((row, rowIndex) => {
      return row.map((tile, colIndex) => {
        return {
          value: tile,
          index: v2(colIndex, rowIndex),
        } as Tile;
      });
    });
  }

  private assignNodeToTile(colIndex: number, rowIndex: number, node: Node) {
    const tile = this.getTile(colIndex, rowIndex);
    if (tile) {
      tile.node = node;
    }
  }

  public generateBoardSprites() {
    this.tileBoard.forEach((row, rowIndex) => {
      row.forEach((tile, colIndex) => {
        const tileSprite = this.getTileSprite(tile);
        if (tileSprite) {
          const { x, y } = this.getTilePosition(colIndex, rowIndex);
          const node = instantiate(tileSprite);
          node.setParent(this.tileNode || this.node);
          node.setPosition(x, y);
          node.active = true;

          node
            .getComponent(BaseTileSprite)
            ?.adjustTexture((colIndex + rowIndex) % 2 === 0);

          this.assignNodeToTile(colIndex, rowIndex, node);
        }
      });
    });
  }

  private getTileSprite(tile: Tile) {
    const tileType = getTileType(tile.value);
    switch (tileType) {
      case TILE_TYPE.WALL: {
        return this.wallSprite?.node;
      }

      default: {
        return this.tileSprite?.node;
      }
    }
  }

  public getTilePosition(colIndex: number, rowIndex: number) {
    const { tileSize } = this;
    return {
      x: colIndex * tileSize,
      y: -rowIndex * tileSize,
    };
  }

  private isAWallTile(tile: Tile) {
    const tileType = getTileType(tile.value);

    return tileType === TILE_TYPE.WALL;
  }

  private isAWalkableTile(tile: Tile) {
    const tileType = getTileType(tile.value);

    return tileType === TILE_TYPE.TILE;
  }

  private getTile(colIndex: number, rowIndex: number) {
    const row = this.tileBoard[rowIndex];
    if (row) {
      return row[colIndex];
    }
    return undefined;
  }

  private isASafeTile(colIndex: number, rowIndex: number) {
    const tile = this.getTile(colIndex, rowIndex);

    return tile !== undefined && !this.isAWallTile(tile);
  }

  public getTileIfSafe(colIndex: number, rowIndex: number) {
    if (this.isASafeTile(colIndex, rowIndex)) {
      return this.getTile(colIndex, rowIndex);
    }
    return undefined;
  }

  public getWalkableTiles(snake: Snake) {
    const snakeTiles = snake.getParts().map((part) => {
      const { x, y } = part.index;

      return `${x}|${y}`;
    });

    return this.tileBoard.reduce((res, row, rowIndex) => {
      const tiles = row.filter((tile, colIndex) => {
        return (
          this.isAWalkableTile(tile) &&
          !snakeTiles.find((v) => v === `${colIndex}|${rowIndex}`)
        );
      });

      res.push(...tiles);

      return res;
    }, new Array<Tile>());
  }

  public getRandomWalkableTile(snake: Snake) {
    const walkableTiles = this.getWalkableTiles(snake);

    return walkableTiles[Math.floor(Math.random() * walkableTiles.length)];
  }

  public spawnRandomFruit(snake: Snake) {
    const { appleSprite, tileNode } = this;
    const tile = this.getRandomWalkableTile(snake);

    if (!appleSprite || !tile.node) return;

    const node = instantiate(appleSprite.node);
    node.setParent(tileNode || this.node);
    node.setPosition(tile.node.position.x, tile.node.position.y);
    node.active = true;

    this.fruits.push({
      node,
      index: tile.index,
    });
  }

  public eatFruit(colIndex: number, rowIndex: number) {
    const fruitIndex = this.fruits.findIndex((v) => {
      const { x, y } = v.index;

      return x === colIndex && y === rowIndex;
    });

    const fruit = this.fruits[fruitIndex];

    if (fruit) {
      fruit.node.destroy();
      this.fruits.splice(fruitIndex, 1);
      return true;
    }

    return false;
  }
}
