import { _decorator, Component, Node } from "cc";
import { SCORE_MANAGER_EVENT } from "../enum/scoreManager";
import { getHighscoreFromLocalStorage } from "../lib/util/localStorage";
const { ccclass } = _decorator;

@ccclass("ScoreManager")
export class ScoreManager extends Component {
  private score = 0;

  private highscore = 0;

  onload() {
    this.node.on(SCORE_MANAGER_EVENT.SCORE_UPDATE, (score: number) => {
      if (score >= this.getHighscore()) {
        this.setHighscore(score);
      }
    });
  }

  public initialize() {
    this.setScore(0);
    this.setHighscore(getHighscoreFromLocalStorage());
  }

  public getScore() {
    return this.score;
  }

  private setScore(score: number) {
    this.score = score;
    this.node.emit(SCORE_MANAGER_EVENT.SCORE_UPDATE, this.score);
  }

  public getHighscore() {
    return this.highscore;
  }

  private setHighscore(highscore: number) {
    this.highscore = highscore;
    this.node.emit(SCORE_MANAGER_EVENT.HIGHSCORE_UPDATE, this.highscore);
  }

  public addScore(score: number) {
    this.setScore(this.score + score);
  }
}
