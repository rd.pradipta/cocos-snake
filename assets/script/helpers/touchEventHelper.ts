import { _decorator, Node } from 'cc';
import { TOUCH_EVENT } from '../enum/touchEvent';

const registerTouchEvent = (nodeToRegister: any) => {
  nodeToRegister.node?.on(Node.EventType.TOUCH_END, () => {
    nodeToRegister.node.emit(TOUCH_EVENT.TOUCH_END);
  });
};

const removeTouchEvent = (nodeToRegister: any) => {
  nodeToRegister.node?.off(Node.EventType.TOUCH_END);
};

export default {
  registerTouchEvent,
  removeTouchEvent,
};
