import { _decorator } from 'cc';

const showNode = (nodeToRegister: any) => {
  nodeToRegister.node.activate = true;
};

const hideNode = (nodeToRegister: any) => {
  nodeToRegister.node.activate = false;
};

export default {
  showNode,
  hideNode,
};
