import {
  _decorator,
  Component,
  Button,
  director,
  find,
  instantiate,
} from "cc";
import { BackgroundSoundtrack } from "../../audio/backgroundSoundtrack";
import { TOUCH_EVENT } from "../../enum/touchEvent";
import { SCENE_KEY } from "../../enum/sceneName";
import { TRANSITION_SCREEN_EVENT } from "../../enum/transitionScreen";
import { BaseButton } from "../../object/baseButton";
import { TransitionScreen } from "../../sprite/transitionScreen";
const { ccclass, property } = _decorator;

@ccclass("TitleScene")
export class TitleScene extends Component {
  private readonly backgroundSoundtrackPersistNodeName =
    "AUDIO_BACKGROUND_SOUNDTRACK_PERSIST";

  @property(Button)
  public readonly playButton?: BaseButton | null;

  @property(BackgroundSoundtrack)
  public readonly backgroundSoundtrack?: BackgroundSoundtrack | null;

  @property(TransitionScreen)
  public readonly transitionScreen?: TransitionScreen | null;

  start() {
    this.spawnBackgroundSoundtrackIfNotExist();
    this.transitionScreen?.fadeOut(0.5);
    this.transitionScreen?.node.once(
      TRANSITION_SCREEN_EVENT.FADE_OUT_COMPLETE,
      () => {
        this.setupPlayButtonClick();
      }
    );
  }

  private spawnBackgroundSoundtrackIfNotExist() {
    const { backgroundSoundtrack, backgroundSoundtrackPersistNodeName } = this;

    if (!backgroundSoundtrack) return;

    if (!find(backgroundSoundtrackPersistNodeName)) {
      const node = instantiate(backgroundSoundtrack.node);
      node.name = backgroundSoundtrackPersistNodeName;
      node.getComponent(BackgroundSoundtrack)?.play();
      director.addPersistRootNode(node);
    }
  }

  private setupPlayButtonClick() {
    this.playButton?.node.on(TOUCH_EVENT.TOUCH_END, () => {
      this.playButton?.unregisterTouchEvent();
      this.goToGameScene();
    });
  }

  private goToGameScene() {
    this.transitionScreen?.fadeIn(0.5);
    this.transitionScreen?.node.once(
      TRANSITION_SCREEN_EVENT.FADE_IN_COMPLETE,
      () => {
        director.loadScene(SCENE_KEY.GAME_SCENE)
      }
    );
  }
}
