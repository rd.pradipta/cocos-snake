import { _decorator } from "cc";
import { ASSET_KEY } from "../../../../lib/enum/asset";
import { BaseSprite } from "../../../../lib/sprite/baseSprite";
const { ccclass } = _decorator;

@ccclass("KeypadLeftSprite")
export class KeypadLeftSprite extends BaseSprite {
  constructor() {
    super("KeypadLeftSprite", ASSET_KEY.IMG_KEYPAD_SPRITESHEET, 3);
  }
}