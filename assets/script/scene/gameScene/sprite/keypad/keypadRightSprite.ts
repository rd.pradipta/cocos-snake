import { _decorator } from "cc";
import { ASSET_KEY } from "../../../../lib/enum/asset";
import { BaseSprite } from "../../../../lib/sprite/baseSprite";
const { ccclass } = _decorator;

@ccclass("KeypadRightSprite")
export class KeypadRightSprite extends BaseSprite {
  constructor() {
    super("KeypadRightSprite", ASSET_KEY.IMG_KEYPAD_SPRITESHEET, 5);
  }
}