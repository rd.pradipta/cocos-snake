import { _decorator } from "cc";
import { ASSET_KEY } from "../../../lib/enum/asset";
import { BaseSprite } from "../../../lib/sprite/baseSprite";
const { ccclass } = _decorator;

@ccclass("AppleSprite")
export class AppleSprite extends BaseSprite {
  constructor() {
    super("AppleSprite", ASSET_KEY.IMG_APPLE_SPRITE);
  }
}
