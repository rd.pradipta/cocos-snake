import { _decorator } from "cc";
import { ASSET_KEY } from "../../../lib/enum/asset";
import { BaseSprite } from "../../../lib/sprite/baseSprite";
const { ccclass } = _decorator;

@ccclass("TrophySprite")
export class TrophySprite extends BaseSprite {
  constructor() {
    super("TrophySprite", ASSET_KEY.IMG_TROPHY_SPRITE);
  }
}
