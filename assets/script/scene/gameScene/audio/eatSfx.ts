import { _decorator, Component, Node } from "cc";
import { BaseAudio } from '../../../lib/audio/baseAudio';
import { ASSET_KEY } from '../../../lib/enum/asset';
const { ccclass } = _decorator;

@ccclass("EatSFX")
export class EatSFX extends BaseAudio {
  constructor() {
    super("EatSFX", ASSET_KEY.AUDIO_EAT_SFX, false, 0.5);
  }
}
