import { _decorator, Component, Node } from "cc";
import { BaseAudio } from '../../../lib/audio/baseAudio';
import { ASSET_KEY } from '../../../lib/enum/asset';
const { ccclass } = _decorator;

@ccclass("TurnSFX")
export class TurnSFX extends BaseAudio {
  constructor() {
    super("TurnSFX", ASSET_KEY.AUDIO_TURN_SFX, false, 0.5);
  }
}
