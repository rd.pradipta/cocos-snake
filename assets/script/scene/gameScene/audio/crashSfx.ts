import { _decorator, Component, Node } from 'cc';
import { BaseAudio } from '../../../lib/audio/baseAudio';
import { ASSET_KEY } from '../../../lib/enum/asset';
const { ccclass } = _decorator;

@ccclass('CrashSFX')
export class CrashSFX extends BaseAudio {  
  constructor() {
    super('CrashSFX', ASSET_KEY.AUDIO_CRASH_SFX, false, 0.5)
  }
}

