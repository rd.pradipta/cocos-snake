import { _decorator, Component, Node } from "cc";
import { ASSET_KEY } from "../lib/enum/asset";
import { BaseSprite } from "../lib/sprite/baseSprite";
const { ccclass } = _decorator;

@ccclass("TitleLogoSprite")
export class TitleLogoSprite extends BaseSprite {
  constructor() {
    super("TitleLogoSprite", ASSET_KEY.IMG_LOGO_SPRITE);
  }
}
