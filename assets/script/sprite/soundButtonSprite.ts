import { _decorator } from "cc";
import { ASSET_KEY } from "../lib/enum/asset";
import { BaseSprite } from "../lib/sprite/baseSprite";
const { ccclass } = _decorator;

@ccclass("SoundButtonSprite")
export class SoundButtonSprite extends BaseSprite {
  private readonly soundOnKey = ASSET_KEY.IMG_SOUND_ON_SPRITE;

  private readonly soundOffKey = ASSET_KEY.IMG_SOUND_OFF_SPRITE;

  constructor() {
    super("SoundButtonSprite", ASSET_KEY.IMG_SOUND_ON_SPRITE);
  }

  public setOn() {
    this.setTexture(ASSET_KEY.IMG_SOUND_ON_SPRITE);
    this.reload();
  }

  public setOff() {
    this.setTexture(ASSET_KEY.IMG_SOUND_OFF_SPRITE);
    this.reload();
  }
}
